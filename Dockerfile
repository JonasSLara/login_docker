FROM php:7.4-apache

# Instalação da extensao pgsql
RUN apt-get update && \
    apt-get install -y libpq-dev && \
    docker-php-ext-install pgsql pdo_pgsql

EXPOSE 80
EXPOSE 443

WORKDIR /var/www/html

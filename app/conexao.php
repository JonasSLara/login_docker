<?php
    $link;
    function connect(){
        global $link;
        $link=null;
        //
        $user="postgres";
        $password="postgres";
        $dbname="postgres";
        $host="meu_postgres";
        $port="5432";
        $link=pg_connect("host=$host port=$port dbname=$dbname user=$user password=$password");
        //
        if(!$link){
            die("erro ao se conectar" . pg_last_error());
        }
    }

    function query($sql){
        global $link;
        $result = null;
        $result = pg_query($link, $sql);
        //
        if(!$result){
            die("erro no query" . pg_last_error());
        }
        return $result;
    }

    function close(){
        global $link;
        pg_close($link);
    }
?>

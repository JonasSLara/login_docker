<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Pagina de login</title>
	<link rel="stylesheet" href="./geral.css">
</head>
<body>
	<nav>
		<ul>
			<li><a href="./cadastrar.php">Criar conta</a></li>
			<li><a href="#">Sobre</a></li>
			<li><a href="https://gitlab.com/users/sign_in">Git Lab</a></li>
		</ul>
	</nav>
	<div>
		<section>
			<img src="./logo.png" alt="logo do gitlab" width="100%" height="180px">
		</section>
		<section id="login">
			<h2>Login</h2>
			<form method="POST" action="controle.php">
				<label for="nome">Nome: </label>
				<input type="text" id="nome" name="nome"><br>
				<label for="senha">Senha: </label>
				<input type="text" id="senha" name="senha"><br>
				<button type="submit" name="op" value="entrar" class="sucesso">Entrar</button>
			</form>
		</section>
	</div>
</body>
</html>

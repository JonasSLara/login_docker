<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>cadastrar</title>
    <link rel="stylesheet" href="./geral.css">
</head>

<body>
    <nav>
        <ul>
            <li><a href="./index.php">entrar</a></li>
            <li><a href="#">Sobre</a></li>
            <li><a href="https://gitlab.com/users/sign_in">Git Lab</a></li>
        </ul>
    </nav>
	<div>
		<section id="cadastrar">
			<h2>Crie sua conta</h2>
			<form method="POST" action="controle.php">
				<label for="nome">Nome: </label>
				<input type="text" id="nome" name="nome"><br>
				<label for="senha">Senha: </label>
				<input type="text" id="senha" name="senha"><br>
				<button type="submit" name="op" value="criar" class="sucesso">Criar</button>
			</form>
		</section>
	</div>
</body>
</html>